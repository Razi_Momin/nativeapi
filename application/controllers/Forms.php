<?php

class Forms extends CI_Controller
{
    public function getRecordsBetween()
    {
        $totalRevenueByDate = 0;
        $totalRevenueByDateValue = 0;
        $resultList = [];
        $nextPage = 0;
        $success = false;
        $nextIdValue = 0;
        $message = 'something went wrong';
        // SELECT * FROM cakeentry WHERE clientDate >= '2020-08-21 00:00:00' AND clientDate <= '2020-08-26 23:59:59'
        $fromDate = trim($this->input->post('fromDate'));
        $toDate = trim($this->input->post('toDate'));
        $nextId = trim($this->input->post('nextId'));
        if ($fromDate === '') {
            $message = "Please enter from date";
        } else if ($toDate === "") {
            $message = "Please enter To date";
        } else {
            $cond = '';
            $cond .= "clientDate >= '$fromDate 00:00:00' AND clientDate <= '$toDate 23:59:59'";
            if ($nextId !== '') {
                $cond .= " AND id > $nextId";
            }
            $limit = OFFSETLIMIT;
            $column = 'cakeName,customerName,cakeWeight,price,clientDate,id';
            $resultList = $this->Common_model->get_records('cakeentry', $column, $cond, $limit, 'clientDate', 'ASC');
            $countOfResult = $this->Common_model->get_records('cakeentry', 'COUNT(id) as totalCount', $cond)[0]['totalCount'];
            if (!empty($resultList)) {
                $offsetCount =  $countOfResult - count($resultList);
                $nextPage =  $offsetCount > 0 ? 1 : 0;
                $totalRevenueByDate = $this->Common_model->get_records('cakeentry', 'SUM(price) as totalRevenue', $cond);
                $totalRevenueByDateValue = $totalRevenueByDate[0]['totalRevenue'];
                $success = true;
                $message = 'Record found';
                $nextIdValue = $resultList[count($resultList) - 1]['id'];
            } else {
                $message = 'No Record found';
            }
        }

        $response['data'] = $resultList;
        $response['totalRevenue'] = $totalRevenueByDateValue;
        $response['totalOrder'] = $countOfResult;
        $response['success'] = $success;
        $response['message'] = $message;
        $response['nextPage'] = $nextPage;
        $response['nextIdValue'] = $nextIdValue;
        echo json_encode($response);
    }
    function postRecords()
    {
        $response = [];
        $success = false;
        $errorMessage = 'Something went wrong';
        $validatin = false;
        // print_r($_POST);
        // exit;
        // if (!empty($_POST)) {
        $cakeName = trim(($this->input->post('cakeName')));
        $customerName = trim($this->input->post('customerName'));
        $cakeWeight = trim($this->input->post('cakeWeight'));
        $price = trim($this->input->post('price'));
        $clientDate = trim($this->input->post('clientDate'));
        if ($cakeName === '') {
            $errorMessage = 'Please enter cake name';
        } else if ($cakeWeight === '') {
            $errorMessage = 'Please enter cake Weight';
        } else if ($customerName === '') {
            $errorMessage = 'Please enter customer name';
        } else if ($price === '') {
            $errorMessage = 'Please enter price';
        } else {
            $errorMessage = 'Entry successfully inserted';
            $data = array(
                'cakeName' => $cakeName,
                'customerName' => $customerName,
                'cakeWeight' => $cakeWeight,
                'price' => $price,
                'clientDate' => $clientDate . ' ' . date("H:i:s")
            );
            $id = $this->db->insert('cakeentry', $data);
            if ($id) {
                $success = true;
            }
        }
        // }
        $response['message'] = $errorMessage;
        $response['success'] = $success;
        echo json_encode($response);
    }
    public function getRecords()
    {
        $response = [];
        $data = [];
        $message = 'something went wrong';
        $success = false;
        $records = "sum(price) as totalRvenue,COUNT(id) as count";
        $result = $this->Common_model->get_row('cakeentry', null, $records);
        if (!empty($result)) {
            $success = true;
            $data = $result;
            $message = 'data found';
        } else {
            $message = 'no record found';
        }
        $response['data'] = $data;
        $response['success'] = $success;
        $response['message'] = $message;
        echo json_encode($response);
    }
    public function getDataByDate()
    {
        $message = 'something went wrong';
        $success = false;
        $result = [];
        $date  = trim(($this->input->post('clientDate')));
        if ($date === '') {
            $message = 'Please enter valid date';
        } else {
            $cond = "sum(price) as totalRvenue,COUNT(id) as count ORDER BY id DESC";
            $cond = "clientDate LIKE '%$date%'";
            $resultList = $this->Common_model->get_records('cakeentry', 'cakeName,customerName,cakeWeight,clientDate,price,id', $cond);
            if (!empty($resultList)) {
                $success = true;
                $result = $resultList;
                $message = 'Record Found';
            } else {
                $message = 'No Record found';
            }
        }
        $response['success'] = $success;
        $response['data'] = $result;
        $response['message'] = $message;
        echo json_encode($response);
    }
    public function deleteEntry()
    {
        $response = [];
        $success = false;
        $message = 'Something went wrong';
        $id = trim($this->input->post('id'));
        if ($id === '') {
            $message = 'Please enter valid ID';
        } else {
            $cond = array('id' => $id);
            $this->Common_model->delete('cakeentry', $cond);
            $returnId = $this->db->affected_rows();
            if ($returnId > 0) {
                $success = true;
                $message = 'Successfully deleted';
            } else {
                $message = 'This id already deleted';
            }
        }
        $response['message'] = $message;
        $response['success'] = $success;
        echo json_encode($response);
    }
}
