<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Common_model extends CI_Model
{

    public function add($tbl = NULL, $record = NULL)
    {
        if (!empty($tbl) && !empty($record)) {
            $this->db->insert($tbl, $record);
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    public function multiple_add($tbl = NULL, $records = NULL)
    {
        if (!empty($tbl) && !empty($records)) {
            $res = $this->db->insert_batch($tbl, $records);
            return $res;
        } else {
            return FALSE;
        }
    }

    public function edit($tbl = NULL, $record = NULL, $cond = NULL)
    {
        if (!empty($tbl) && !empty($record) && !empty($cond)) {
            $this->db->where($cond);
            $res = $this->db->update($tbl, $record);
            return $res;
        } else {
            return FALSE;
        }
    }

    public function multiple_edit($tbl = NULL, $record = NULL, $col = NULL)
    {
        if (!empty($tbl) && !empty($record)) {
            $res = $this->db->update_batch($tbl, $record, $col);
            return $res;
        } else {
            return FALSE;
        }
    }

    public function delete($tbl = NULL, $cond = NULL)
    {
        if (!empty($tbl) && !empty($cond)) {
            $this->db->where($cond);
            $res = $this->db->delete($tbl);
            return $res;
        } else {
            return FALSE;
        }
    }

    public function multiple_delete($tbl = NULL, $column = NULL, $value = NULL)
    {
        if (!empty($tbl) && !empty($column) && !empty($value)) {
            $this->db->where_in($column, $value);
            $res = $this->db->delete($tbl);
            return $res;
        } else {
            return FALSE;
        }
    }

    public function get_row($tbl = NULL, $cond = NULL, $cols = NULL)
    {
        if (!empty($tbl)) {
            if (!empty($cols)) {
                $this->db->select($cols);
            }
            if (!empty($cond)) {
                $res = $this->db->get_where($tbl, $cond)->row_array();
            } else {
                $res = $this->db->get($tbl)->row_array();
            }
            return $res;
        } else {
            return FALSE;
        }
    }

    public function get_records($tbl = NULL, $cols = NULL, $cond = NULL, $limit = NULL, $order_col = NULL, $order_by = NULL)
    {

        if (!empty($tbl)) {
            if (!empty($cols)) {
                $this->db->select($cols);
            }
            if (!empty($limit)) {
                $this->db->limit($limit);
            }
            if (!empty($order_col) && !empty($order_by)) {
                $this->db->order_by($order_col, $order_by);
            }
            if (!empty($cond)) {
                $this->db->where($cond);
            }

            $res = $this->db->get($tbl)->result_array();
            // echo $this->db->last_query();
            // exit;
            return $res;
            //            echo $this->db->last_query();
            //            exit;
        } else {
            return FALSE;
        }
    }

    public function search_by_like($tbl = NULL, $column = NULL, $match = NULL, $cols = NULL, $cond = NULL, $limit = NULL)
    {
        if (!empty($tbl)) {
            if (!empty($cols)) {
                $this->db->select($cols);
            }
            if (!empty($limit)) {
                $this->db->limit($limit);
            }
            if (!empty($column) && !empty($match)) {
                $this->db->like($column, $match);
            }
            if (!empty($cond)) {
                $this->db->where($cond);
            }
            $res = $this->db->get($tbl)->result_array();
            return $res;
        } else {
            return FALSE;
        }
    }
}
